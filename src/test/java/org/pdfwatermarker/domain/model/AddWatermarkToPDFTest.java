package org.pdfwatermarker.domain.model;

import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.builder.AddWatermarkInformationBuilder;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AddWatermarkToPDFTest {

    private static PDFOperationsRepository pdfOperationsRepository;

    @BeforeClass
    public static void beforeClass() {
        pdfOperationsRepository = mock(PDFOperationsRepository.class);
    }

    @Test
    public void execute() throws WatermarkPDFException {
        doNothing().when(pdfOperationsRepository).addWatermark(any(AddWatermarkInformation.class));

        AddWatermarkInformation addWatermarkInformation = new AddWatermarkInformationBuilder()
                .createAddWatermarkInformation();

        AddWatermarkToPDF addWatermarkToPDF = new AddWatermarkToPDF(pdfOperationsRepository);

        addWatermarkToPDF.execute(addWatermarkInformation);

        assertTrue(true);
    }
}