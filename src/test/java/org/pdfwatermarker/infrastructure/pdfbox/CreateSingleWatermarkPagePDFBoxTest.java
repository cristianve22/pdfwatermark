package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.FileRepository;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.WatermarkImageSize;
import org.pdfwatermarker.domain.valueobjects.WatermarkPositionEnum;
import org.pdfwatermarker.domain.valueobjects.builder.CreateWatermarkInformationBuilder;
import org.pdfwatermarker.infrastructure.fs.FSFileRepository;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateSingleWatermarkPagePDFBoxTest {

    private static final String IMAGE_FILE_PNG = "star.png";
    private static final String IMAGE_FILE_JPG = "star.jpg";
    private static final String IMAGE_FILE_TIF = "star.tif";

    private static List<String> tmpFilePathList;

    private static PDFBoxDocument originalDocument;
    private static FileRepository fileRepository;
    private static CreateWatermarkInformation createWatermarkCenterPNG;
    private static CreateWatermarkInformation createWatermarkCenterJPG;
    private static CreateWatermarkInformation createWatermarkCenterTIF;
    private static CreateWatermarkInformation createWatermarkTopLeftPNG;
    private static CreateWatermarkInformation createWatermarkTopRightPNG;
    private static CreateWatermarkInformation createWatermarkBottomLeftPNG;
    private static CreateWatermarkInformation createWatermarkBottomRightPNG;

    @BeforeClass
    public static void setUp() {
        URL urlFile = CreateSingleWatermarkPagePDFBoxTest.class.getClassLoader().getResource(IMAGE_FILE_PNG);
        assertNotNull(urlFile);
        String imagePathPng = urlFile.getPath();

        URL urlFileJpg = CreateSingleWatermarkPagePDFBoxTest.class.getClassLoader().getResource(IMAGE_FILE_JPG);
        assertNotNull(urlFileJpg);
        String imagePathJpg = urlFileJpg.getPath();

        URL urlFileTif = CreateSingleWatermarkPagePDFBoxTest.class.getClassLoader().getResource(IMAGE_FILE_TIF);
        assertNotNull(urlFileTif);
        String imagePathTif = urlFileTif.getPath();

        PDPage a4Page = new PDPage(PDRectangle.A4);
        PDRectangle a4PageSize = a4Page.getMediaBox();

        WatermarkImageSize watermarkImageSize = new WatermarkImageSize(200, 200);

        tmpFilePathList = new ArrayList<>();

        originalDocument = mock(PDFBoxDocument.class);
        when(originalDocument.getPageMediaBox(anyInt())).thenReturn(a4PageSize);

        fileRepository = new FSFileRepository();

        createWatermarkCenterPNG = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathPng)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.CENTER)
                .createCreateWatermarkInformation();

        createWatermarkCenterJPG = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathJpg)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.CENTER)
                .createCreateWatermarkInformation();

        createWatermarkCenterTIF = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathTif)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.CENTER)
                .createCreateWatermarkInformation();

        createWatermarkTopLeftPNG = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathPng)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.TOP_LEFT)
                .createCreateWatermarkInformation();

        createWatermarkTopRightPNG = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathPng)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.TOP_RIGHT)
                .createCreateWatermarkInformation();

        createWatermarkBottomLeftPNG = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathPng)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.BOTTOM_LEFT)
                .createCreateWatermarkInformation();

        createWatermarkBottomRightPNG = new CreateWatermarkInformationBuilder()
                .setImageWatermarkPath(imagePathPng)
                .setWatermarkImageSize(watermarkImageSize)
                .setWatermarkPositionEnum(WatermarkPositionEnum.BOTTOM_RIGHT)
                .createCreateWatermarkInformation();
    }

    @AfterClass
    public static void tearDown() {
        tmpFilePathList.forEach(tmpFilePath -> {
            try {
                Files.deleteIfExists(Path.of(tmpFilePath));
            } catch (IOException ignored) {
            }
        });
    }

    @Test
    public void createWatermarkCenterPage() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        String tmpFilePath = createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkCenterPNG, 1);

        assertNotNull(tmpFilePath);
        assertTrue(Files.exists(Path.of(tmpFilePath), LinkOption.NOFOLLOW_LINKS));

        tmpFilePathList.add(tmpFilePath);
    }

    @Test
    public void createWatermarkCenterPageJpg() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        String tmpFilePath = createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkCenterJPG, 1);

        assertNotNull(tmpFilePath);
        assertTrue(Files.exists(Path.of(tmpFilePath), LinkOption.NOFOLLOW_LINKS));

        tmpFilePathList.add(tmpFilePath);
    }

    @Test(expected = WatermarkPDFException.class)
    public void createWatermarkCenterPageTif() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkCenterTIF, 1);
    }

    @Test
    public void createWatermarkTopLeftPage() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        String tmpFilePath = createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkTopLeftPNG, 1);

        assertNotNull(tmpFilePath);
        assertTrue(Files.exists(Path.of(tmpFilePath), LinkOption.NOFOLLOW_LINKS));

        tmpFilePathList.add(tmpFilePath);
    }

    @Test
    public void createWatermarkTopRightPage() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        String tmpFilePath = createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkTopRightPNG, 1);

        assertNotNull(tmpFilePath);
        assertTrue(Files.exists(Path.of(tmpFilePath), LinkOption.NOFOLLOW_LINKS));

        tmpFilePathList.add(tmpFilePath);
    }

    @Test
    public void createWatermarkBottomLeftPage() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        String tmpFilePath = createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkBottomLeftPNG, 1);

        assertNotNull(tmpFilePath);
        assertTrue(Files.exists(Path.of(tmpFilePath), LinkOption.NOFOLLOW_LINKS));

        tmpFilePathList.add(tmpFilePath);
    }

    @Test
    public void createWatermarkBottomRightPage() throws Exception {
        CreateSingleWatermarkPagePDFBox createWatermarkSinglePagePDFBox = new CreateSingleWatermarkPagePDFBox(fileRepository);
        String tmpFilePath = createWatermarkSinglePagePDFBox.createWatermarkPage(originalDocument, createWatermarkBottomRightPNG, 1);

        assertNotNull(tmpFilePath);
        assertTrue(Files.exists(Path.of(tmpFilePath), LinkOption.NOFOLLOW_LINKS));

        tmpFilePathList.add(tmpFilePath);
    }
}