package org.pdfwatermarker.infrastructure.pdfbox;

import org.junit.*;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.WatermarkOverlayPosition;
import org.pdfwatermarker.domain.valueobjects.builder.AddWatermarkInformationBuilder;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AddWatermarkToPDFPagesPDFBoxTest {

    private static final String ORIGINAL_SINGLE_PAGE_PDF_NAME = "test.pdf";
    private static final String ORIGINAL_MULTIPLE_PAGES_PDF_NAME = "test-multipage.pdf";
    private static final String ORIGINAL_WATERMARK_CENTER_PDF_NAME = "star-center.pdf";
    private static final String ORIGINAL_WATERMARK_UPPER_LEFT_PDF_NAME = "star-upper-left.pdf";
    private static final String ORIGINAL_WATERMARK_UPPER_RIGHT_PDF_NAME = "star-upper-right.pdf";
    private static final String ORIGINAL_WATERMARK_LOWER_LEFT_PDF_NAME = "star-lower-left.pdf";
    private static final String ORIGINAL_WATERMARK_LOWER_RIGHT_PDF_NAME = "star-lower-right.pdf";
    private static final String WATERMARKED_FILE_NAME = "watermarked.pdf";

    private static String watermarkedPDFPath;
    private static Path watermarkTmpDir;

    private static AddWatermarkInformation singlePageCenterBackground;
    private static AddWatermarkInformation singlePageCenterForeground;
    private static AddWatermarkInformation singlePageUpperLeftForeground;
    private static AddWatermarkInformation singlePageUpperRightForeground;
    private static AddWatermarkInformation singlePageLowerLeftForeground;
    private static AddWatermarkInformation singlePageLowerRightForeground;
    private static AddWatermarkInformation multiplePagesCenterBackground;
    private static AddWatermarkInformation multiplePagesCenterForeground;

    private final AddWatermarkToPDFPagesPDFBox addWatermarkToPDFWithPDFBox = new AddWatermarkToPDFPagesPDFBox();

    @BeforeClass
    public static void beforeClass() throws Exception {
        URL urlFileSinglePage = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_SINGLE_PAGE_PDF_NAME);
        assertNotNull(urlFileSinglePage);

        URL urlFileMultiPage = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_MULTIPLE_PAGES_PDF_NAME);
        assertNotNull(urlFileMultiPage);

        URL urlCenterWatermarkFile = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_WATERMARK_CENTER_PDF_NAME);
        assertNotNull(urlCenterWatermarkFile);
        Map<Integer, String> watermarkSinglePagePdfMap = new HashMap<>();
        watermarkSinglePagePdfMap.put(1, urlCenterWatermarkFile.getPath());
        Map<Integer, String> watermarkMultiplePagesPdfMap = new HashMap<>();
        watermarkMultiplePagesPdfMap.put(1, urlCenterWatermarkFile.getPath());
        watermarkMultiplePagesPdfMap.put(3, urlCenterWatermarkFile.getPath());
        watermarkMultiplePagesPdfMap.put(5, urlCenterWatermarkFile.getPath());
        watermarkMultiplePagesPdfMap.put(7, urlCenterWatermarkFile.getPath());

        URL urlUpperLeftWatermarkFile = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_WATERMARK_UPPER_LEFT_PDF_NAME);
        assertNotNull(urlUpperLeftWatermarkFile);
        Map<Integer, String> watermarkSingleUpperLeftPagePdfMap = new HashMap<>();
        watermarkSingleUpperLeftPagePdfMap.put(1, urlUpperLeftWatermarkFile.getPath());

        URL urlUpperRightWatermarkFile = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_WATERMARK_UPPER_RIGHT_PDF_NAME);
        assertNotNull(urlUpperRightWatermarkFile);
        Map<Integer, String> watermarkSingleUpperRightPagePdfMap = new HashMap<>();
        watermarkSingleUpperRightPagePdfMap.put(1, urlUpperRightWatermarkFile.getPath());

        URL urlLowerLeftWatermarkFile = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_WATERMARK_LOWER_LEFT_PDF_NAME);
        assertNotNull(urlLowerLeftWatermarkFile);
        Map<Integer, String> watermarkSingleLowerLeftPagePdfMap = new HashMap<>();
        watermarkSingleLowerLeftPagePdfMap.put(1, urlLowerLeftWatermarkFile.getPath());

        URL urlLowerRightWatermarkFile = AddWatermarkToPDFPagesPDFBoxTest.class.getClassLoader().getResource(ORIGINAL_WATERMARK_LOWER_RIGHT_PDF_NAME);
        assertNotNull(urlLowerRightWatermarkFile);
        Map<Integer, String> watermarkSingleLowerRightPagePdfMap = new HashMap<>();
        watermarkSingleLowerRightPagePdfMap.put(1, urlLowerRightWatermarkFile.getPath());

        watermarkTmpDir = Files.createTempDirectory("watermark");
        watermarkedPDFPath = watermarkTmpDir.toAbsolutePath() + "/" + WATERMARKED_FILE_NAME;

        singlePageCenterBackground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkSinglePagePdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.BACKGROUND)
                .createAddWatermarkInformation();

        singlePageCenterForeground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkSinglePagePdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.FOREGROUND)
                .createAddWatermarkInformation();

        singlePageUpperLeftForeground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkSingleUpperLeftPagePdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.FOREGROUND)
                .createAddWatermarkInformation();

        singlePageUpperRightForeground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkSingleUpperRightPagePdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.FOREGROUND)
                .createAddWatermarkInformation();

        singlePageLowerLeftForeground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkSingleLowerLeftPagePdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.FOREGROUND)
                .createAddWatermarkInformation();

        singlePageLowerRightForeground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileSinglePage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkSingleLowerRightPagePdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.FOREGROUND)
                .createAddWatermarkInformation();

        multiplePagesCenterBackground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileMultiPage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkMultiplePagesPdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.BACKGROUND)
                .createAddWatermarkInformation();

        multiplePagesCenterForeground = new AddWatermarkInformationBuilder()
                .setOriginalPDFPath(urlFileMultiPage.getPath())
                .setWatermarkedPDFPath(watermarkedPDFPath)
                .setWatermarkedPages(watermarkMultiplePagesPdfMap)
                .setWatermarkOverlayPosition(WatermarkOverlayPosition.FOREGROUND)
                .createAddWatermarkInformation();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        Files.deleteIfExists(watermarkTmpDir);
    }

    @After
    public void tearDown() throws Exception {
        Files.deleteIfExists(Path.of(watermarkedPDFPath));
    }

    @Test
    public void executeSinglePageCenterBackground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(singlePageCenterBackground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeSinglePageCenterForeground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(singlePageCenterForeground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeSinglePageUpperLeftForeground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(singlePageUpperLeftForeground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeSinglePageUpperRightForeground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(singlePageUpperRightForeground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeSinglePageLowerLeftForeground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(singlePageLowerLeftForeground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeSinglePageLowerRightForeground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(singlePageLowerRightForeground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeMultiplePagesCenterBackground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(multiplePagesCenterBackground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }

    @Test
    public void executeMultiplePagesCenterForeground() throws WatermarkPDFException {
        addWatermarkToPDFWithPDFBox.addWatermark(multiplePagesCenterForeground);

        assertTrue(new File(watermarkedPDFPath).exists());
    }
}