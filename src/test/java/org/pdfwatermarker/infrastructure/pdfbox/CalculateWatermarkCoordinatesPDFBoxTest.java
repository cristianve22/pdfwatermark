package org.pdfwatermarker.infrastructure.pdfbox;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.valueobjects.WatermarkPositionCoordinates;
import org.pdfwatermarker.domain.valueobjects.WatermarkPositionEnum;

import java.net.URL;

import static org.junit.Assert.*;

public class CalculateWatermarkCoordinatesPDFBoxTest {

    private static final String IMAGE_FILE_PNG = "star.png";

    private static final float PAGE_WIDTH = 400f;
    private static final float PAGE_HEIGHT = 800f;
    private static final float IMAGE_WIDTH = 200f;
    private static final float IMAGE_HEIGHT = 200f;

    private static CalculateWatermarkCoordinatesPDFBox calculateWatermarkCoordinatesPDFBox;

    @BeforeClass
    public static void beforeClass() throws Exception {
        PDRectangle pageRectangle = new PDRectangle(PAGE_WIDTH, PAGE_HEIGHT);

        URL urlFile = CreateSingleWatermarkPagePDFBoxTest.class.getClassLoader().getResource(IMAGE_FILE_PNG);
        assertNotNull(urlFile);
        String imagePathPng = urlFile.getPath();
        PDDocument pdDocument = new PDDocument();
        PDImageXObject imageObject = PDImageXObject.createFromFile(imagePathPng, pdDocument);

        calculateWatermarkCoordinatesPDFBox = new CalculateWatermarkCoordinatesPDFBox(pageRectangle, imageObject);
    }

    @Test
    public void getCoordinatesCenter() {
        WatermarkPositionCoordinates coordinates =
                calculateWatermarkCoordinatesPDFBox.getCoordinates(WatermarkPositionEnum.CENTER);

        assertNotNull(coordinates);
        assertEquals((PAGE_WIDTH/2) - (IMAGE_WIDTH/2), coordinates.getXAxis(), 0f);
        assertEquals((PAGE_HEIGHT/2) - (IMAGE_HEIGHT/2), coordinates.getYAxis(), 0f);
    }

    @Test
    public void getCoordinatesTopLeft() {
        WatermarkPositionCoordinates coordinates =
                calculateWatermarkCoordinatesPDFBox.getCoordinates(WatermarkPositionEnum.TOP_LEFT);

        assertNotNull(coordinates);
        assertEquals(0f, coordinates.getXAxis(), 0f);
        assertEquals(PAGE_HEIGHT - IMAGE_HEIGHT, coordinates.getYAxis(), 0f);
    }

    @Test
    public void getCoordinatesTopRight() {
        WatermarkPositionCoordinates coordinates =
                calculateWatermarkCoordinatesPDFBox.getCoordinates(WatermarkPositionEnum.TOP_RIGHT);

        assertNotNull(coordinates);
        assertEquals(PAGE_WIDTH - IMAGE_WIDTH, coordinates.getXAxis(), 0f);
        assertEquals(PAGE_HEIGHT - IMAGE_HEIGHT, coordinates.getYAxis(), 0f);
    }

    @Test
    public void getCoordinatesBottomLeft() {
        WatermarkPositionCoordinates coordinates =
                calculateWatermarkCoordinatesPDFBox.getCoordinates(WatermarkPositionEnum.BOTTOM_LEFT);

        assertNotNull(coordinates);
        assertEquals(0, coordinates.getXAxis(), 0f);
        assertEquals(0, coordinates.getYAxis(), 0f);
    }

    @Test
    public void getCoordinatesBottomRight() {
        WatermarkPositionCoordinates coordinates =
                calculateWatermarkCoordinatesPDFBox.getCoordinates(WatermarkPositionEnum.BOTTOM_RIGHT);

        assertNotNull(coordinates);
        assertEquals(PAGE_WIDTH - IMAGE_WIDTH, coordinates.getXAxis(), 0f);
        assertEquals(0, coordinates.getYAxis(), 0f);
    }
}