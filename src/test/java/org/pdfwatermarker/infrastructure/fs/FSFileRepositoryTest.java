package org.pdfwatermarker.infrastructure.fs;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import java.nio.file.Files;
import java.nio.file.Path;

public class FSFileRepositoryTest {

    private static FSFileRepository fsFileRepository;

    private static Path tempFilePath;
    private static Path tempDirectory;

    @BeforeClass
    public static void beforeClass() throws Exception {
        fsFileRepository = new FSFileRepository();

        tempFilePath = Files.createTempFile("original", "pdf");
        tempDirectory = Files.createTempDirectory("watermark-pdf");
    }

    @AfterClass
    public static void afterClass() throws Exception {
        Files.deleteIfExists(tempFilePath);
        Files.deleteIfExists(tempDirectory);
    }

    @Test(expected = WatermarkPDFException.class)
    public void shouldReturnAnExceptionWhenOriginalPathIsEmpty() throws WatermarkPDFException {
        fsFileRepository.checkFileExist("");
    }

    @Test()
    public void shouldPassTestWhenFileExistInOriginalPath() throws WatermarkPDFException {
        fsFileRepository.checkFileExist(tempFilePath.toString());
    }

    @Test(expected = WatermarkPDFException.class)
    public void shouldReturnAnExceptionWhenFolderPathIsEmpty() throws WatermarkPDFException {
        fsFileRepository.checkFolderExist("");
    }

    @Test()
    public void shouldPassTestWhenFolderExist() throws WatermarkPDFException {
        fsFileRepository.checkFolderExist(tempDirectory.toString());
    }

    @Test()
    public void shouldPassTestWhenFileIsCreatedCorrectly() throws WatermarkPDFException {
        fsFileRepository.createTmpFile();
    }

}
