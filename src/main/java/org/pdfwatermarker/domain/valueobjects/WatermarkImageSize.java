package org.pdfwatermarker.domain.valueobjects;

public class WatermarkImageSize {

    private final int height;
    private final int width;

    public WatermarkImageSize(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
