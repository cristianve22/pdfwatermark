package org.pdfwatermarker.domain.valueobjects;

public class WatermarkPositionCoordinates {

    private final float xAxis;
    private final float yAxis;

    public WatermarkPositionCoordinates(float xAxis, float yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public float getXAxis() {
        return xAxis;
    }

    public float getYAxis() {
        return yAxis;
    }
}
