package org.pdfwatermarker.domain.valueobjects;

public enum WatermarkOverlayPosition {
    BACKGROUND,
    FOREGROUND
}
