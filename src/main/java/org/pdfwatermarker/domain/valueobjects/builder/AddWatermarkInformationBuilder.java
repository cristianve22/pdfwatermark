package org.pdfwatermarker.domain.valueobjects.builder;

import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.WatermarkOverlayPosition;

import java.util.Map;

public class AddWatermarkInformationBuilder {
    private String originalPDFPath;
    private String watermarkedPDFPath;
    private Map<Integer, String> watermarkedPages;
    private WatermarkOverlayPosition watermarkOverlayPosition;

    public AddWatermarkInformationBuilder setOriginalPDFPath(String originalPDFPath) {
        this.originalPDFPath = originalPDFPath;
        return this;
    }

    public AddWatermarkInformationBuilder setWatermarkedPDFPath(String watermarkedPDFPath) {
        this.watermarkedPDFPath = watermarkedPDFPath;
        return this;
    }

    public AddWatermarkInformationBuilder setWatermarkedPages(Map<Integer, String> watermarkedPages) {
        this.watermarkedPages = watermarkedPages;
        return this;
    }

    public AddWatermarkInformationBuilder setWatermarkOverlayPosition(WatermarkOverlayPosition watermarkOverlayPosition) {
        this.watermarkOverlayPosition = watermarkOverlayPosition;
        return this;
    }

    public AddWatermarkInformation createAddWatermarkInformation() {
        return new AddWatermarkInformation(originalPDFPath, watermarkedPDFPath, watermarkedPages, watermarkOverlayPosition);
    }
}