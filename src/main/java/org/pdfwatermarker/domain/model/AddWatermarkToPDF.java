package org.pdfwatermarker.domain.model;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;

public class AddWatermarkToPDF {

    private final PDFOperationsRepository pdfOperationsRepository;

    public AddWatermarkToPDF(PDFOperationsRepository pdfOperationsRepository) {
        this.pdfOperationsRepository = pdfOperationsRepository;
    }

    public void execute(AddWatermarkInformation addWatermarkInformation) throws WatermarkPDFException {
        pdfOperationsRepository.addWatermark(addWatermarkInformation);
    }
}
