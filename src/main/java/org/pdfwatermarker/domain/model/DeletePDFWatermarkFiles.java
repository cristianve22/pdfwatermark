package org.pdfwatermarker.domain.model;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;

import java.util.Map;

/**
 * Delete watermark PDF page once already used
 */
public class DeletePDFWatermarkFiles {

    private final PDFOperationsRepository pdfOperationsRepository;

    public DeletePDFWatermarkFiles(PDFOperationsRepository pdfOperationsRepository) {
        this.pdfOperationsRepository = pdfOperationsRepository;
    }

    public void execute(Map<Integer, String> watermarkedPages) throws WatermarkPDFException {
        this.pdfOperationsRepository.deletePDFWatermarkFiles(watermarkedPages);
    }
}
