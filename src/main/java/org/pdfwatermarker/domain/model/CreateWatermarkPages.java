package org.pdfwatermarker.domain.model;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;

import java.util.Map;

public class CreateWatermarkPages {

    private final PDFOperationsRepository pdfOperationsRepository;

    public CreateWatermarkPages(PDFOperationsRepository pdfOperationsRepository) {
        this.pdfOperationsRepository = pdfOperationsRepository;
    }

    public Map<Integer, String> execute(CreateWatermarkInformation createWatermarkInformation)
            throws WatermarkPDFException {

        return pdfOperationsRepository.createPages(createWatermarkInformation);

    }
}
