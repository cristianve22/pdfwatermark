package org.pdfwatermarker.domain.repository;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import java.io.File;

public interface FileRepository {

    void checkFileExist(String path) throws WatermarkPDFException;

    void checkFolderExist(String path) throws WatermarkPDFException;

    File createTmpFile() throws WatermarkPDFException;

}
