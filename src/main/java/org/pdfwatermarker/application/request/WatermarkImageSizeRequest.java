package org.pdfwatermarker.application.request;

public class WatermarkImageSizeRequest {

    private final int height;
    private final int width;

    public WatermarkImageSizeRequest(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
