package org.pdfwatermarker.application.request;

public enum OverlayPositionEnumRequest {
    BACKGROUND,
    FOREGROUND
}
