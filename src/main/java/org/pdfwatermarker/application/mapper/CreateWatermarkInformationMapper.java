package org.pdfwatermarker.application.mapper;

import org.pdfwatermarker.application.request.OverlayPositionEnumRequest;
import org.pdfwatermarker.application.request.PDFWatermarkRequest;
import org.pdfwatermarker.application.request.WatermarkImageSizeRequest;
import org.pdfwatermarker.application.request.WatermarkPositionEnumRequest;
import org.pdfwatermarker.domain.valueobjects.*;
import org.pdfwatermarker.domain.valueobjects.builder.CreateWatermarkInformationBuilder;

public class CreateWatermarkInformationMapper {

    private CreateWatermarkInformationMapper() {
        //private default constructor
    }

    public static CreateWatermarkInformation map(PDFWatermarkRequest pdfWatermarkRequest) {

        return new CreateWatermarkInformationBuilder()
                .setOriginalPDFPath(pdfWatermarkRequest.getOriginalPDFPath())
                .setPagesToWatermark(pdfWatermarkRequest.getPagesToWatermark())
                .setImageWatermarkPath(pdfWatermarkRequest.getImageWatermarkPath())
                .setWatermarkImageSize(mapImageSize(pdfWatermarkRequest.getWatermarkImageSize()))
                .setWatermarkPositionEnum(mapWatermarkPosition(pdfWatermarkRequest.getWatermarkPositionEnum()))
                .setWatermarkOverlayPosition(mapOverlayPosition(pdfWatermarkRequest.getOverlayPositionEnum()))
                .createCreateWatermarkInformation();
    }

    private static WatermarkImageSize mapImageSize(WatermarkImageSizeRequest watermarkImageSizeRequest) {
        return new WatermarkImageSize(watermarkImageSizeRequest.getHeight(), watermarkImageSizeRequest.getWidth());
    }

    private static WatermarkPositionEnum mapWatermarkPosition(WatermarkPositionEnumRequest watermarkPositionEnumRequest) {
        return WatermarkPositionEnum.valueOf(watermarkPositionEnumRequest.toString());
    }

    private static WatermarkOverlayPosition mapOverlayPosition(OverlayPositionEnumRequest overlayPositionEnumRequest) {
        return WatermarkOverlayPosition.valueOf(overlayPositionEnumRequest.toString());
    }
}
