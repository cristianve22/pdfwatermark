package org.pdfwatermarker.application;

import org.pdfwatermarker.application.mapper.AddWatermarkInformationMapper;
import org.pdfwatermarker.application.mapper.CreateWatermarkInformationMapper;
import org.pdfwatermarker.application.request.PDFWatermarkRequest;
import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.model.AddWatermarkToPDF;
import org.pdfwatermarker.domain.model.CheckRequestData;
import org.pdfwatermarker.domain.model.CreateWatermarkPages;
import org.pdfwatermarker.domain.model.DeletePDFWatermarkFiles;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;

import java.util.Map;

public class CreatePDFWatermark {

    private final CreateWatermarkPages createWatermarkPages;
    private final AddWatermarkToPDF addWatermarkToPDF;
    private final CheckRequestData checkRequestData;
    private final DeletePDFWatermarkFiles deletePDFWatermarkFiles;

    public CreatePDFWatermark(CreateWatermarkPages createWatermarkPages, AddWatermarkToPDF addWatermarkToPDF, CheckRequestData checkRequestData, DeletePDFWatermarkFiles deletePDFWatermarkFiles) {
        this.createWatermarkPages = createWatermarkPages;
        this.addWatermarkToPDF = addWatermarkToPDF;
        this.checkRequestData = checkRequestData;
        this.deletePDFWatermarkFiles = deletePDFWatermarkFiles;
    }

    public void execute(PDFWatermarkRequest pdfWatermarkRequest) throws WatermarkPDFException {
        //Validate Data
        validateData(pdfWatermarkRequest);

        //Get List of watermarked pages
        Map<Integer, String> watermarkedPages = getWatermarkedPages(pdfWatermarkRequest);

        //Add watermark to original PDF
        addWatermarkToPDF(pdfWatermarkRequest, watermarkedPages);

        //Delete list of watermarked pages (extra)
        deleteWatermarkFiles(watermarkedPages);

    }

    private void deleteWatermarkFiles(Map<Integer, String> watermarkedPages) throws WatermarkPDFException {
        this.deletePDFWatermarkFiles.execute(watermarkedPages);
    }

    private void validateData(PDFWatermarkRequest pdfWatermarkRequest) throws WatermarkPDFException {
        this.checkRequestData.checkFile(pdfWatermarkRequest.getOriginalPDFPath());
        this.checkRequestData.checkFile(pdfWatermarkRequest.getImageWatermarkPath());
        this.checkRequestData.checkTargetFile(pdfWatermarkRequest.getWatermarkedPDFPath());
        this.checkRequestData.checkValidPagesToWatermark(pdfWatermarkRequest.getPagesToWatermark());
    }

    private Map<Integer, String> getWatermarkedPages(PDFWatermarkRequest pdfWatermarkRequest) throws WatermarkPDFException {

        CreateWatermarkInformation watermarkInfo = CreateWatermarkInformationMapper.map(pdfWatermarkRequest);
        return this.createWatermarkPages.execute(watermarkInfo);
    }

    private void addWatermarkToPDF(PDFWatermarkRequest pdfWatermarkRequest, Map<Integer, String> watermarkedPages) throws WatermarkPDFException {

        AddWatermarkInformation addWatermarkInformation = AddWatermarkInformationMapper.map(pdfWatermarkRequest, watermarkedPages);
        this.addWatermarkToPDF.execute(addWatermarkInformation);
    }
}
