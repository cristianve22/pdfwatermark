package org.pdfwatermarker.infrastructure.configuration;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;

import java.util.Map;
import java.util.Properties;

public class PDFWatermarkProperties {

    public static final String KEY_PDF_LIBRARY = "pdf.library";
    public static final String KEY_FILE_REPOSITORY = "file.repository";

    public static final String DEFAULT_PDF_LIBRARY = "pdfbox";
    public static final String DEFAULT_FILE_REPOSITORY = "fs";

    private static final Properties properties = new Properties();

    private PDFWatermarkProperties() {
        //Default private constructor
    }

    public static void load(Map<String, String> propertiesMap) throws WatermarkPDFException {
        if (propertiesMap == null) {
            throw new WatermarkPDFException("Null map for properties");
        }
        properties.putAll(propertiesMap);
    }

    public static String getPDFLibrary() {
        return properties.getProperty(KEY_PDF_LIBRARY, DEFAULT_PDF_LIBRARY);
    }

    public static String getFileRepository() {
        return properties.getProperty(KEY_FILE_REPOSITORY, DEFAULT_FILE_REPOSITORY);
    }

}
