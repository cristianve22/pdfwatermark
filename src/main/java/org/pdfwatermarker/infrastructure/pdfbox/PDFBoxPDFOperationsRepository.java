package org.pdfwatermarker.infrastructure.pdfbox;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.repository.PDFOperationsRepository;
import org.pdfwatermarker.domain.valueobjects.AddWatermarkInformation;
import org.pdfwatermarker.domain.valueobjects.CreateWatermarkInformation;

import java.util.Map;

public class PDFBoxPDFOperationsRepository implements PDFOperationsRepository {

    private final CreateAllWatermarkPagesPDFBox createAllWatermarkPagesPDFBox;
    private final AddWatermarkToPDFPagesPDFBox addWatermarkToPDFPagesPDFBox;
    private final DeleteWatermarkPages deleteWatermarkPages;

    public PDFBoxPDFOperationsRepository(CreateAllWatermarkPagesPDFBox createAllWatermarkPagesPDFBox, AddWatermarkToPDFPagesPDFBox addWatermarkToPDFPagesPDFBox,
            DeleteWatermarkPages deleteWatermarkPages) {
        this.createAllWatermarkPagesPDFBox = createAllWatermarkPagesPDFBox;
        this.addWatermarkToPDFPagesPDFBox = addWatermarkToPDFPagesPDFBox;
        this.deleteWatermarkPages = deleteWatermarkPages;
    }

    @Override
    public Map<Integer, String> createPages(CreateWatermarkInformation createWatermarkInformation) throws WatermarkPDFException {
        return this.createAllWatermarkPagesPDFBox.createPages(createWatermarkInformation);
    }

    @Override
    public void addWatermark(AddWatermarkInformation addWatermarkInformation) throws WatermarkPDFException {
        this.addWatermarkToPDFPagesPDFBox.addWatermark(addWatermarkInformation);
    }

    @Override
    public void deletePDFWatermarkFiles(Map<Integer, String> watermarkedPages) {
        this.deleteWatermarkPages.deleteWatermarkPages(watermarkedPages);
    }
}
