package org.pdfwatermarker.infrastructure.pdfbox;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class DeleteWatermarkPages {

    public void deleteWatermarkPages(Map<Integer, String> watermarkedPages) {
        watermarkedPages.forEach((k, v) -> {
            Path path = Paths.get(v);
            try {
                Files.deleteIfExists(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
