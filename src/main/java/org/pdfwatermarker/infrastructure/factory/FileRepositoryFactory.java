package org.pdfwatermarker.infrastructure.factory;

import org.pdfwatermarker.domain.exception.WatermarkPDFException;
import org.pdfwatermarker.domain.model.CheckRequestData;
import org.pdfwatermarker.domain.repository.FileRepository;

public class FileRepositoryFactory extends AbstractFactory{

    private FileRepositoryFactory() {
        //Default private constructor
    }

    public static CheckRequestData getInstance() throws WatermarkPDFException {
        FileRepository fileRepository = loadFileRepository();
        return new CheckRequestData(fileRepository);
    }
}
