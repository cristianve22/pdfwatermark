# PDF Watermark 🎓🏭
# La Salle Ramon Llull – MDAS 🤖

# Contenido 📇
* 1. Aplicación
  * 1.1. Casos de uso
* 2. Dominio
  * 2.1. Modelos
  * 2.2. Repositorios
* 3. Infraestructura
  * 3.1. PDFBox
  * 3.2. FileSystem
  * 3.3. Factory
* 4. Instalación del proyecto
* 5. Diagrama de classes

# Arquitectura Hexagonal - DDD

![Hexagonal Image](/doc/hexa.png)

# 1. Aplicación

## 1.1. Casos de uso

# 2. Domain

## 2.1. Modelos
## 2.2. Repositorios

# 3. Infraestructura

## 3.1. PDFBox
## 3.2. FileSystem
## 3.3. Factory


# 4. Instalación/Configuración proyecto ⚙️

### Step - 1️⃣ Descargar el proyecto (clone). ``git clone https://gitlab.com/joanmdas/pdfwatermark.git``
### Step - 2️⃣ Posicionate en la ruta de la carpeta (CMD/Terminal) ``javac main.java``


# 5. Diagrama de classes (Diseño del Software) ✍️

![Diagrama UML](/doc/UML.png)
